﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GalaSoft.MvvmLight.Validation
{
#if DEBUG
    public class AllwaysNotValid : ValidationAttribute
    {
        public AllwaysNotValid():base("{0} value is allways false ;)"){ }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            return new ValidationResult(base.FormatErrorMessage(validationContext.MemberName), new string[]{validationContext.MemberName});
        }
    }
#endif
}
