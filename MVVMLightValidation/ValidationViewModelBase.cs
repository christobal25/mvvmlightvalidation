﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace GalaSoft.MvvmLight
{
    /// <inheritdoc cref="ViewModelBase" />
    /// <summary>
    /// This is an extension of the MVVMLight <see cref="ViewModelBase"/>, 
    /// which directly implements the <see cref="INotifyDataErrorInfo"/>.
    /// </summary>
    public abstract class ValidationViewModelBase : ViewModelBase, INotifyDataErrorInfo
    {
        
        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;

        protected void NotifyErrorsChanged(string propertyName)
        {
            ErrorsChanged?.Invoke(this, new DataErrorsChangedEventArgs(propertyName));
        }

        private readonly IDictionary<string, IList<string>> _errors = new Dictionary<string, IList<string>>();

        public IEnumerable GetErrors(string propertyName)
        {
            if (!_errors.ContainsKey(propertyName)) yield break;
            var propertyErrors = _errors[propertyName];
            foreach (var propertyError in propertyErrors)
            {
                yield return propertyError;
            }
        }

        public bool HasErrors => _errors.Count > 0;

        protected void ValidateProperty(string propertyName, object value)
        {
            ViewModelBase objectToValidate = this;
            var results = new List<ValidationResult>();
            var isValid = Validator.TryValidateProperty(
                value,
                new ValidationContext(objectToValidate, null, null)
                {
                    MemberName = propertyName
                },
                results);

            if (isValid)
                RemoveErrorsForProperty(propertyName);
            else
                AddErrorsForProperty(propertyName, results);

            NotifyErrorsChanged(propertyName);
        }

        private void AddErrorsForProperty(string propertyName, IEnumerable<ValidationResult> validationResults)
        {
            RemoveErrorsForProperty(propertyName);
            _errors.Add(propertyName, validationResults.Select(vr => vr.ErrorMessage).ToList());
        }

        private void RemoveErrorsForProperty(string propertyName)
        {
            if (_errors.ContainsKey(propertyName))
                _errors.Remove(propertyName);
        }

        public bool ValidateObject(object validateThis)
        {
            var objectToValidate = validateThis;
            _errors.Clear();
            var results = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(objectToValidate,
                new ValidationContext(objectToValidate, null, null), results,
                true);
            if (!isValid)
            {
                var propertiesWithMessage = results.SelectMany(vr => vr.MemberNames);
                foreach (var property in propertiesWithMessage)
                {
                    var messages = results.Where(vr => vr.MemberNames.Contains(property));
                    AddErrorsForProperty(property, messages);
                    NotifyErrorsChanged(property);
                }
            }
            return isValid;
        }
    }
}
