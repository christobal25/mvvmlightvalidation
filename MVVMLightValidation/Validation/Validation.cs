﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GalaSoft.MvvmLight.Validation
{
    /// <inheritdoc />
    /// <summary>
    /// This attribute specifies a property to be validated.
    /// </summary>
    public class Validation : Attribute
    {
    }
}
