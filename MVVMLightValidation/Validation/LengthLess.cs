﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GalaSoft.MvvmLight.Validation
{

    /// <inheritdoc />
    /// <summary>
    /// Validates a string after it is shorter than specified
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = true)]
    public class LengthLess : ValidationAttribute
    {
        /// <inheritdoc />
        /// <summary>
        /// Validates a string after it is shorter than specified
        /// </summary>
        /// <param name="length">Maximum length of the string</param>
        public LengthLess(int length)
        {
            Length = length;
        }

        /// <summary>
        /// Validates a string after it is shorter than specified
        /// </summary>
        /// <param name="length">Maximum length of the string</param>
        /// <param name="alternativName">Alternate name for the field</param>
        public LengthLess(int length, string alternativName)
        {
            Length = length;
            AlternativName = alternativName;
        }

        /// <summary>
        /// Validates a string after it is shorter than specified
        /// </summary>
        /// <param name="length">Maximum length of the string</param>
        /// <param name="alternativName">Alternate name for the field</param>
        /// <param name="alternativError">like <c>"{0} message {1}"</c> where 0 is the Fieldname and 1 the <c>length</c></param>
        public LengthLess(int length, string alternativName, string alternativError)
        {
            Length = length;
            AlternativName = alternativName;
            AlternativErrorString = alternativError;
        }


        private string AlternativName { get; set; }

        private string AlternativErrorString { get; set; }
        public int Length { get; }

        public override bool IsValid(object value)
        {
            var content = (string) value;           
            return content.Length <= Length;
        }

        public override string FormatErrorMessage(string name)
        {
            if (!string.IsNullOrEmpty(AlternativName) && string.IsNullOrEmpty(AlternativErrorString))
            {
                return string.Format(CultureInfo.CurrentUICulture, Resources.Resource.LengthLess, AlternativName, Length);
            }
            else if(!string.IsNullOrEmpty(AlternativErrorString))
            {
                return string.Format(CultureInfo.CurrentUICulture, AlternativErrorString, AlternativName, Length);
            }    
            return string.Format(CultureInfo.CurrentUICulture, Resources.Resource.LengthLess, name, Length);
        }
    }
}
