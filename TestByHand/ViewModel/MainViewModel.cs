using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Validation;
using System.ComponentModel.DataAnnotations;

namespace TestByHand.ViewModel
{
    public class MainViewModel : ValidationViewModelBase
    {
        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel()
        {
       
        }

        private string _testString = "";
        [Validation]
        [LengthLess(10, "Name Mayer", "{0} darf maximal {1} Buchstaben haben")]
        public string TestString
        {
            get => _testString;
            set => Set(nameof(TestString), ref _testString, value);
        }

        private void ExecuteAdd()
        {
            
        }

        public bool CanExecuteAdd()
        {
            return ValidateObject(this);
        }

        private RelayCommand _addCommand;
        public ICommand AddCommand => _addCommand ?? (_addCommand = new RelayCommand(ExecuteAdd, CanExecuteAdd));
    }
}