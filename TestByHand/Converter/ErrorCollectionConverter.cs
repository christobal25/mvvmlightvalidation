﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;

namespace TestByHand.Converter
{
    public class ErrorCollectionConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null) return null;
            var toolTip = new ListBox();
            var items = (ReadOnlyObservableCollection<ValidationError>)value;
            toolTip.ItemsSource = items;
            toolTip.Background = Brushes.LightSalmon;
            toolTip.DisplayMemberPath = "ErrorContent";
            toolTip.MinWidth = 150;
            //return new ListBox
            //{
            //    ItemsSource = (ReadOnlyObservableCollection<ValidationError>)value,
            //    BorderThickness = new Thickness(0),
            //    Background = Brushes.Transparent,
            //    DisplayMemberPath = "ErrorContent"
            //};
            return toolTip;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
